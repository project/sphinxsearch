<?php

namespace Drupal\sphinxsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Sphinx Search settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sphinxsearch_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sphinxsearch.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['connection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Connection settings'),
    ];

    $form['connection']['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $this->config('sphinxsearch.settings')->get('host') ?? 'localhost',
      '#required' => TRUE,
    ];

    $form['connection']['port'] = [
      '#type' => 'number',
      '#title' => $this->t('Port'),
      '#default_value' => $this->config('sphinxsearch.settings')->get('port') ?? '9306',
      '#required' => TRUE,
    ];

    $form['connection']['index'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search index'),
      '#default_value' => $this->config('sphinxsearch.settings')->get('index') ?? 'drupal',
      '#required' => TRUE,
    ];

    $form['search'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Search settings'),
    ];

    $form['search']['items_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Items per page'),
      '#default_value' => $this->config('sphinxsearch.settings')->get('items_per_page') ?? '15',
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /*
    @todo Add validate for settings? Example:
    if ($form_state->getValue('example') != 'example') {
    $form_state->setErrorByName('example',
    $this->t('The value is not correct.'));
    }
     */
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('sphinxsearch.settings')
      ->set('host', $form_state->getValue('host'))
      ->save();

    $this->config('sphinxsearch.settings')
      ->set('port', $form_state->getValue('port'))
      ->save();

    $this->config('sphinxsearch.settings')
      ->set('index', $form_state->getValue('index'))
      ->save();

    $this->config('sphinxsearch.settings')
      ->set('items_per_page', $form_state->getValue('items_per_page'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
